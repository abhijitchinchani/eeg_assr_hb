clear; clc; close all hidden;

Toolbox_Path = pwd;
addpath(genpath([Toolbox_Path '/Code_library/Userdefined']));
InitToolboxPath(Toolbox_Path);

Subs = [1:42 44:46];
ASSR_freq = 40;
Noise_freq = 5;
freq_err = 0.1;

Fs = 1000;
SubType = readcell('Data_HB/Subject_type.csv');
SubType = [SubType{:,3}];
SubSham = find(SubType == 0);
SubStim40 = find(SubType == 40);
SubStim11 = find(SubType == 11);

analyse_flag = false;
save_path = sprintf('Data_HB/log_SNR_Analysis/'); mkdir(save_path);
plot_path = sprintf('Plots/log_SNR_Analysis/'); mkdir(plot_path);
k = 7;

%% Load data and analyse

if analyse_flag
    
    for sub = 1:length(Subs)
        data_path = sprintf('Data_HB/Preprocess/HB_%d/',Subs(sub));
        
        SubData = load(fullfile(data_path,sprintf('HB_%d_Preproc.mat',Subs(sub))));
        SubCPCAData = load(fullfile(data_path,sprintf('HB_%d_CPCA_G_H.mat',Subs(sub))));
        
        SNR_struct(sub).raw_T0 = getSnrStruct(SubData.TrialsT0, ASSR_freq, Noise_freq, freq_err, Fs);
        SNR_struct(sub).raw_T1 = getSnrStruct(SubData.TrialsT1, ASSR_freq, Noise_freq, freq_err, Fs);

        Comps = nt_mmat(permute(SubData.TrialsT0, [2,1,3]), SubCPCAData.CPCA_GH_T0.VS(:,1:k));
        SNR_struct(sub).GH_T0 = getSnrStruct(permute(Comps, [2,1,3]), ASSR_freq, Noise_freq, freq_err, Fs);
        
        Comps = nt_mmat(permute(SubData.TrialsT1, [2,1,3]), SubCPCAData.CPCA_GH_T1.VS(:,1:k));
        SNR_struct(sub).GH_T1 = getSnrStruct(permute(Comps, [2,1,3]), ASSR_freq, Noise_freq, freq_err, Fs);
        
        Comps = nt_mmat(permute(SubData.TrialsT0, [2,1,3]), SubCPCAData.CPCA_GH_T0_T1.VS(:,1:k));
        SNR_struct(sub).GH_comb_T0 = getSnrStruct(permute(Comps, [2,1,3]), ASSR_freq, Noise_freq, freq_err, Fs);
        Comps = nt_mmat(permute(SubData.TrialsT1, [2,1,3]), SubCPCAData.CPCA_GH_T0_T1.VS(:,1:k));
        SNR_struct(sub).GH_comb_T1 = getSnrStruct(permute(Comps, [2,1,3]), ASSR_freq, Noise_freq, freq_err, Fs);
        
        fprintf('CPCA GH Analysis: Sub %d Completed \n', Subs(sub));
    end
    
    save(fullfile(save_path, 'logSNR.mat'),'SNR_struct');
    
end

%% Plots

load(fullfile(save_path, 'logSNR.mat'),'SNR_struct');

data = {cat(1, SNR_struct.raw_T0), cat(1, SNR_struct.raw_T1)};
plotHeatMap(data, 'Ch', 'Sub', [0 0 1920 1080], {'Pre', 'Post'}, 'Raw EEG', plot_path);
plottopoplot(data, [0 0 1920 1080], {'Pre', 'Post'}, 'Raw EEG', plot_path);

data = {cat(1, SNR_struct(SubSham).raw_T0), cat(1, SNR_struct(SubSham).raw_T1);...
    cat(1, SNR_struct(SubStim11).raw_T0), cat(1, SNR_struct(SubStim11).raw_T1);...
    cat(1, SNR_struct(SubStim40).raw_T0), cat(1, SNR_struct(SubStim40).raw_T1)};
titleStr = {'Pre Sham', 'Post Sham';'Pre Stim 11', 'Post Stim 11';'Pre Stim 40', 'Post Stim 40'};
plottopoplot(data', [0 0 1920 1080], titleStr', 'Raw EEG Groupwise', plot_path);

data = {cat(1, SNR_struct.GH_T0), cat(1, SNR_struct.GH_T1)};
plotHeatMap(data, 'Comp', 'Sub', [0 0 1920 1080], {'Pre', 'Post'}, 'CPCA GH separate', plot_path);

data = {cat(1, SNR_struct.GH_comb_T0), cat(1, SNR_struct.GH_comb_T1)};
plotHeatMap(data, 'Comp', 'Sub', [0 0 1920 1080], {'Pre', 'Post'}, 'CPCA GH combined', plot_path);

%% Functions

function SNR = getSnrStruct(data, ASSR_freq, Noise_freq, freq_err, Fs)

params.Fs = Fs;
params.fpass = [2 55];
params.tapers = [1 1];
params.pad = -1;

for kk = 1:size(data,1)
    [S(:,kk), f(:,kk)] = mtspectrumc(nanmean(data(kk,:,:), 3), params);
end

f = f(:,1);
SNR = S(abs(f-ASSR_freq) < freq_err,:)./nanmean(S(abs(f-ASSR_freq) > freq_err & abs(f-ASSR_freq) < Noise_freq,:),1);

end

function [] = plotHeatMap(data, XStr, YStr, Pos, titleStr, supStr, plot_path)

% data = Subjects x Channels/Components

fig = figure;
set(fig, 'Position', Pos);

for ii = 1:length(data)
    temp = data{ii};
%     temp(temp<0) = 0;
%     temp = temp./nansum(temp,2);
    
    subplot(1, length(data), ii);
    h = heatmap(temp); colormap hot;
    if ii == 1
        aa = ['h.YData = sprintfc(' sprintf('''%s',YStr) '{%d}'', 1:size(temp,1));'];
        eval(aa);
    end
    aa = ['h.XData = sprintfc(' sprintf('''%s',XStr) '{%d}'', 1:size(temp,2));'];
    eval(aa);
    title(titleStr{ii});
end

suptitle(sprintf('SNR: %s', supStr));
set(findall(gcf,'-property','FontSize'),'FontSize',15);
saveas(fig,[plot_path sprintf('SNR_%s.png',strrep(supStr, ' ','_'))]);

end

function [] = plottopoplot(data, Pos, titleStr, supStr, plot_path)

% data = Subjects x Channels/Components

EEGLoc = 'cog_GSN-HydroCel-256.sfp';

fig = figure;
set(fig, 'Position', Pos);

for jj = 1:size(data,1)
    for ii = 1:size(data,2)
        temp = geomean(data{jj,ii},1);
        
        subplot(size(data,1), size(data,2), (jj-1)*size(data,2)+ii);
        h = topoplot(temp,EEGLoc,'electrodes','on','colormap','hot');
        ax(jj, ii) = gca;
        title(titleStr{jj, ii});
        set(gca, 'CLim', [1 3.2]);
        cbar; colormap hot;
    end
end

suptitle(sprintf('SNR: %s', supStr));
set(findall(gcf,'-property','FontSize'),'FontSize',15);
saveas(fig,[plot_path sprintf('SNR_topoplot_%s.png',strrep(supStr, ' ','_'))]);

end

