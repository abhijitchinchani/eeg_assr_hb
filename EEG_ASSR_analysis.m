clear; clc; close all;

Toolbox_Path = pwd;
addpath(genpath([Toolbox_Path '/Code_library/Userdefined']));
InitToolboxPath(Toolbox_Path);
plot_path = 'Plots/';

data = ft_read_data('Data/pilot1_hb_ASSRt0_20190125_031019.raw');
events = ft_read_event('Data/pilot1_hb_ASSRt0_20190125_031019.raw');
Fs = 1000; 
data = data(1:256,:);
TimeOfTrial = 2;

[Trials, goodSensors] = Preprocess_Epoch(data, Fs, events, 'aeSt', TimeOfTrial);

fig = figure;
emo_plot_mt(nanmean(Trials(goodSensors,:,:),3),Fs,[2 50]);
xlabel('Frequency in Hz');
ylabel('Power');
title('Spectrum of ASSR at 40Hz');
set(findall(gcf,'-property','FontSize'),'FontSize',15)
saveas(fig,[plot_path 'ASSR_Spectrum_P01_t0.png']);

%-----------------------------------------------------
%% CPCA - Estimate use G matrix to estimate components
%-----------------------------------------------------

fMax = 45; k = 5;

Z = permute(Trials(goodSensors,:,:), [2,1,3]); N = size(Z,1);
Sz = fft(Z.*hanning(N));
Sz = abs(Sz(1:round(N/2),:,:));
Sz = Sz - nanmean(Sz,1);

f = (0:1/TimeOfTrial:(Fs-1)/2)';
Sz = Sz(f<fMax,:,:); f = f(f<fMax);

Sz = permute(Sz, [1,3,2]);
Sz_big = reshape(Sz, [size(Sz,1)*size(Sz,2), size(Sz,3)]);
f_big = repmat(f,[size(Sz,2),1]);

G = [f_big>=2 & f_big<4, f_big>=4 & f_big<8, f_big>=8 & f_big<14,...
    f_big>=14 & f_big<25, f_big>=25 & f_big<40, f_big==40, f_big>40]; 

[~,~,~, D] = runCPCA(Sz_big, 1, 1, k); 
[~,~,UG, DG, VG,~,~,UE, DE, VE] = runCPCA(Sz_big, G, 1, k); 

plotScree(DG, 10);
VarG = buildVarianceTable(DG, DE, D);

fig = figure;
Sig_Perm_Code_CP(Sz_big, G, VarG);
saveas(fig, 'Significance.png');

%% G Matrix Components

P_corr = corrcoef([UG(:,1:k), G]); 
P_corr_mat = P_corr(1:k,k+1:end); % 

fig = figure; 
set(fig, 'Position', [0 0 1920 1080]);
h = heatmap(abs(P_corr_mat));
h.YData = {'Comp 1','Comp 2','Comp 3', 'Comp 4', 'Comp 5'};
h.XData = {'Delta (2-4Hz)', 'Theta (4-8Hz)', 'Alpha (8-14Hz)', 'Beta (14-25Hz)', 'Gamma (25-39Hz)', 'ASSR (40Hz)', 'High gamma (>40Hz)'};
colormap hot;
suptitle('Preditctor Correlations');
set(findall(gcf,'-property','FontSize'),'FontSize',15)
saveas(fig,[plot_path 'P_corr_G_P01_t0.png']);

W = NaN(256,k);
W(goodSensors,:) = VG(:,1:k);
Comps = nt_mmat(Z, VG(:,1:k));

PlotComp(permute(Comps,[2,1,3]), W, Fs, k, 'titleStr','G Matrix','plot_path', plot_path);

%% Residue Components

P_corr = corrcoef([UE(:,1:k), G]); 
P_corr_mat = P_corr(1:k,k+1:end); % 

fig = figure; 
set(fig, 'Position', [0 0 1920 1080]);
h = heatmap(abs(P_corr_mat));
h.YData = {'Comp 1','Comp 2','Comp 3', 'Comp 4', 'Comp 5'};
h.XData = {'Delta (2-4Hz)', 'Theta (4-8Hz)', 'Alpha (8-14Hz)', 'Beta (14-25Hz)', 'Gamma (25-39Hz)', 'ASSR (40Hz)', 'High gamma (>40Hz)'};

colormap hot;
set(findall(gcf,'-property','FontSize'),'FontSize',15)
saveas(fig,[plot_path 'P_corr_E_P01_t0.png']);

W = NaN(256,k);
W(goodSensors,:) = VE(:,1:k);
Comps = nt_mmat(Z, VE(:,1:k));

PlotComp(permute(Comps,[2,1,3]), W, Fs, k, 'titleStr','Residue','plot_path', plot_path);

%% H-Matrix analysis analog

Z = permute(Trials(goodSensors,:,:), [2,3,1]); N = size(Z,1);
Z_time_big = reshape(Z,[size(Z,1)*size(Z,2),size(Z,3)]);

H_analog = VG(:,1:k);

[~,~,~, D] = runCPCA(Z_time_big, 1, 1, k); 
[~,~,UHa, DHa, VHa,~,~,UE, DE, VE] = runCPCA(Z_time_big, 1, H_analog, k); 

P_corr = corrcoef([VHa(:,1:k), H_analog]); 
P_corr_mat = P_corr(1:k,k+1:end); % 

fig = figure; 
set(fig, 'Position', [0 0 1920 1080]);
h = heatmap(abs(P_corr_mat));
h.YData = {'Comp 1','Comp 2','Comp 3', 'Comp 4', 'Comp 5'};
h.XData = {'H1', 'H2', 'H3', 'H4', 'H5'};
colormap hot;
suptitle('Preditctor Correlations');
set(findall(gcf,'-property','FontSize'),'FontSize',15)
saveas(fig,[plot_path 'P_corr_H_analog_time_P01_t0.png']);

W = NaN(256,k);
W(goodSensors,:) = VHa(:,1:k);
Comps = nt_mmat(permute(Z,[1,3,2]), VHa(:,1:k));

PlotComp(permute(Comps,[2,1,3]), W, Fs, k, 'titleStr','Ha Matrix','plot_path', plot_path);

plotScree(DHa, 10);
VarG = buildVarianceTable(DHa, DE, D);

%% H Matrix digital

Z = permute(Trials(goodSensors,:,:), [2,3,1]); N = size(Z,1);
Z_time_big = reshape(Z,[size(Z,1)*size(Z,2),size(Z,3)]);

H_digital = zeros(size(VG(:,1:k)));
for kk = 1:k
    temp = VG(:,kk); SD = 0.8*std(VG(:,kk)); m = mean(VG(:,kk));
    idx = (temp > m+SD) | (temp< m-SD);
    H_digital(idx,kk) = 1;
end

[~,~,UHd, DHd, VHd,~,~,UE, DE, VE] = runCPCA(Z_time_big, 1, H_digital, k); 

P_corr = corrcoef([VHd(:,1:k), H_digital]); 
P_corr_mat = P_corr(1:k,k+1:end); % 

fig = figure; 
set(fig, 'Position', [0 0 1920 1080]);
h = heatmap(abs(P_corr_mat));
h.YData = {'Comp 1','Comp 2','Comp 3', 'Comp 4', 'Comp 5'};
h.XData = {'H1', 'H2', 'H3', 'H4', 'H5'};
colormap hot;
suptitle('Preditctor Correlations');
set(findall(gcf,'-property','FontSize'),'FontSize',15)
saveas(fig,[plot_path 'P_corr_H_analog_time_P01_t0.png']);

W = NaN(256,k);
W(goodSensors,:) = VHd(:,1:k);
Comps = nt_mmat(permute(Z,[1,3,2]), VHd(:,1:k));

PlotComp(permute(Comps,[2,1,3]), W, Fs, k, 'titleStr','Hd Matrix','plot_path', plot_path);