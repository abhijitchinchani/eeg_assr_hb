clear; clc; close all hidden;

Toolbox_Path = pwd;
addpath(genpath([Toolbox_Path '/Code_library/Userdefined']));
InitToolboxPath(Toolbox_Path);

Subs = [1:42 44:46];
ASSR_freq = 40;
Noise_freq = 5;
freq_err = 0.1;

Fs = 1000;
SubType = readcell('Data_HB/Subject_type.csv');
SubType = [SubType{:,3}];
SubSham = find(SubType == 0);
SubStim40 = find(SubType == 40);
SubStim11 = find(SubType == 11);

analyse_flag = false;
save_path = sprintf('Data_HB/CPCA_GH_T0_T1_combined/Average_Sub/'); mkdir(save_path);

%% Load data and analyse

if analyse_flag
    
    for sub = 1:length(Subs)
        data_path = sprintf('Data_HB/Preprocess/HB_%d/',Subs(sub));
        plot_path = sprintf('Plots/CPCA_GH_T0_T1_combined/HB_%d/',Subs(sub)); mkdir(plot_path);
        
        SubData = load(fullfile(data_path,sprintf('HB_%d_Preproc.mat',Subs(sub))));
        SubCPCAData = load(fullfile(data_path,sprintf('HB_%d_CPCA_G_H.mat',Subs(sub))));
        
        data = cat(3, SubData.TrialsT0, SubData.TrialsT1);
        
        k = SubCPCAData.CPCA_GH_T0_T1.k;
        W = SubCPCAData.CPCA_GH_T0_T1.VS(:,1:k);
        Comps = nt_mmat(permute(data, [2,1,3]), SubCPCAData.CPCA_GH_T0_T1.VS(:,1:k));
        [S, f] = PlotComp(permute(Comps,[2,1,3]), W, SubData.Fs, k, ...
            'titleStr',sprintf('HB %d T0+T1 GH',Subs(sub)),'plot_path', plot_path);
        f = f(:,1);
        SNR = S(abs(f-ASSR_freq) < freq_err,:)./nanmean(S(abs(f-ASSR_freq) > freq_err & abs(f-ASSR_freq) < Noise_freq,:),1);
        n = find(SNR == max(SNR)); close all;
        
        k = SubCPCAData.CPCA_GH_T0_T1.k;
        W = SubCPCAData.CPCA_GH_T0_T1.VS(:,1:k);
        Comps = nt_mmat(permute(SubData.TrialsT0, [2,1,3]), SubCPCAData.CPCA_GH_T0_T1.VS(:,1:k));
        BaselineComps = nt_mmat(permute(SubData.BaselineT0, [2,1,3]), SubCPCAData.CPCA_GH_T0_T1.VS(:,1:k));
        
        PlotComp(permute(Comps,[2,1,3]), W, SubData.Fs, k, ...
            'titleStr',sprintf('HB %d T0 GH',Subs(sub)),'plot_path', plot_path);
        AvgComp(:,sub,1) = nanmean(Comps(:,n,:),3);
        AvgBaseline(:,sub,1) = nanmean(BaselineComps(:,n,:),3);
        AvgW(:,sub,1) = W(:,n);
        SelComp(sub,1) = n;
        
        k = SubCPCAData.CPCA_GH_T0_T1.k;
        W = SubCPCAData.CPCA_GH_T0_T1.VS(:,1:k);
        Comps = nt_mmat(permute(SubData.TrialsT1, [2,1,3]), SubCPCAData.CPCA_GH_T0_T1.VS(:,1:k));
        BaselineComps = nt_mmat(permute(SubData.BaselineT1, [2,1,3]), SubCPCAData.CPCA_GH_T0_T1.VS(:,1:k));
        
        PlotComp(permute(Comps,[2,1,3]), W, SubData.Fs, k, ...
            'titleStr',sprintf('HB %d T1 GH',Subs(sub)),'plot_path', plot_path);
        AvgComp(:,sub,2) = nanmean(Comps(:,n,:),3);
        AvgBaseline(:,sub,2) = nanmean(BaselineComps(:,n,:),3);
        AvgW(:,sub,2) = W(:,n);
        SelComp(sub,2) = n;
        
        clear SubData SubCPCAData;
        fprintf('CPCA GH Analysis: Sub %d Completed \n', Subs(sub));
    end
    
    save(fullfile(save_path, 'SelectedComponents.mat'),'AvgComp','AvgBaseline','AvgW','SelComp');
    
end

%% Subject average and plots

load(fullfile(save_path, 'SelectedComponents.mat'),'AvgComp','AvgBaseline','AvgW','SelComp');

for sub = 1:length(Subs)
    temp = AvgComp(:,sub,:);
    AvgComp(:,sub,:) = (AvgComp(:,sub,:) - nanmean(temp(:)))./nanstd(temp(:));
    AvgBaseline(:,sub,:) = (AvgBaseline(:,sub,:) - nanmean(temp(:)))./nanstd(temp(:));
    for ii = 1:2
        w = AvgW(:,sub,ii);
        [~, idx] = max(abs(w));
        AvgComp(:,sub,ii) = AvgComp(:,sub,ii)*sign(w(idx));
        AvgBaseline(:,sub,ii) = AvgBaseline(:,sub,ii)*sign(w(idx));
        
        AvgW(:,sub,ii) = AvgW(:,sub,ii)*sign(w(idx));
    end
end


%% Sham vs Stim

plot_path = sprintf('Plots/CPCA_GH_T0_T1_combined/Average_Sub/'); mkdir(plot_path);

params.Fs = Fs;
params.fpass = [2 55];
params.tapers = [1 1];
params.pad = -1;

for sub = 1:length(Subs)
    for ii = 1:2
        data = AvgComp(1:Fs,sub,ii);
        [S, f] = mtspectrumc(data, params);
        ASSR_Power(ii,sub) = S(f == ASSR_freq);
        ASSR_SNR(ii,sub) = S(abs(f-ASSR_freq) < freq_err,:)./nanmean(S(abs(f-ASSR_freq) > freq_err & abs(f-ASSR_freq) < Noise_freq,:),1);
        
        baseline = AvgBaseline(1:Fs,sub,ii);
        [Sb, fb] = mtspectrumc(baseline, params);
        ASSR_baseline(ii,sub) = S(abs(f-ASSR_freq) < freq_err,:)-Sb(abs(fb-ASSR_freq) < freq_err,:);
    end
end

fig = figure;
histogram(nanmean(ASSR_SNR,1), 10);
xlabel('SNR'); ylabel('#');
title('SNR Histogram');
saveas(fig,[plot_path sprintf('ASSR_SNR_Hist.png')]);

idx = nanmean(ASSR_SNR,1) < 4;
ASSR_Power(:,idx) = NaN;
ASSR_SNR(:,idx) = NaN;
ASSR_baseline(:,idx) = NaN;
AvgComp(:,idx,:) = NaN;
AvgW(:,idx,:) = NaN;

%%

fig = figure;
set(fig, 'Position', [0 0 1920 1080]);

s = PlotCompCol(permute(AvgComp(:,SubSham,1),[3,1,2]), nanmean(AvgW(:,SubSham,1),2), Fs, [6 1], ...
    'titleStr',sprintf('Pre Sham'));
s = PlotCompCol(permute(AvgComp(:,SubStim11,1),[3,1,2]), nanmean(AvgW(:,SubStim11,1),2), Fs, [6 2], ...
    'titleStr',sprintf('Pre Stim 11'),'s', s);
s = PlotCompCol(permute(AvgComp(:,SubStim40,1),[3,1,2]), nanmean(AvgW(:,SubStim40,1),2), Fs, [6 3], ...
    'titleStr',sprintf('Pre Stim 40'),'s', s);

s = PlotCompCol(permute(AvgComp(:,SubSham,2),[3,1,2]), nanmean(AvgW(:,SubSham,2),2), Fs, [6 4], ...
    'titleStr',sprintf('Post Sham'),'s', s);
s = PlotCompCol(permute(AvgComp(:,SubStim11,2),[3,1,2]), nanmean(AvgW(:,SubStim11,2),2), Fs, [6 5], ...
    'titleStr',sprintf('Post Stim 11'),'s', s);
s = PlotCompCol(permute(AvgComp(:,SubStim40,2),[3,1,2]), nanmean(AvgW(:,SubStim40,2),2), Fs, [6 6], ...
    'titleStr',sprintf('Post Stim 40'),'s', s);

linkaxes(s(1,:));
linkaxes(s(2,:));
linkaxes(s(3,:));

set(findall(gcf,'-property','FontSize'),'FontSize',12);
saveas(fig,[plot_path sprintf('Results_ASSR_plots.png')]);


plotShamStimResults(ASSR_Power, SubSham, SubStim11, SubStim40, 'ASSR Power', 'ASSR Power Analysis separately for each subject', plot_path);
plotShamStimResults(ASSR_SNR, SubSham, SubStim11, SubStim40, 'ASSR SNR', 'ASSR SNR Analysis separately for each subject', plot_path);
plotShamStimResults(ASSR_baseline, SubSham, SubStim11, SubStim40, 'ASSR baseline SNR', 'ASSR baseline SNR Analysis separately for each subject', plot_path);

%% Functions

function [p,p1,figh] = cog_anova(data, group, effects, varnames)

% Analysis of variance for testing main and interaction effects of multiple hypotheses

anova_in = reshape(data,[],1); % reshapes tensor to single column along first dim (SSVEP)
size_val = size(data);

if ~exist('group','var') || isempty(group)
    for i=1:ndims(data)
        group(:,i) = repmat(reshape(ones(prod(size_val(1:i-1)),1)*(1:size(data,i)),[],1),prod(size_val(i+1:end)),1);
    end
end

matrix = zeros(length(effects),size(group,2));

for i=1:length(effects)
    matrix(i,effects{i}) = 1; % Set as one for those main and interaction effects that need to be tested
end

figure;
[p,~,stats,~,figh] = anovan(anova_in,group,'Random',3,'varnames',varnames,'model','interaction'); % Perform anova
p1 = multcompare(stats);

end

function [p,p1] = plotShamStimResults(data, Sham, Stim11, Stim40, YlabelStr, titleStr, plot_path)

X(:,:,1) = data(:,Sham); % Sham
X(:,:,2) = data(:,Stim11); % Stim 11
X(:,:,3) = data(:,Stim40); % Stim 40

X = permute(X, [1,3,2]);

[p,p1, figh] = cog_anova(X, [], {1,2,[1 2]}, {'Pre/Post','Sham/Stim','Subjects'});
saveas(figh,[plot_path sprintf('%s_ANOVA.png',titleStr)]);

X(3,:,:) = X(2,:,:) - X(1,:,:);

fig = figure;
set(fig, 'Position',[0 0 1000 500]);

h = barwitherr(nanstd(X,[],3)/sqrt(size(X,3)), nanmean(X,3));
set(gca, 'XTickLabels', {'Pre','Post','Post - Pre'});
ylabel({YlabelStr});
legend(h,{'Sham','Stim 11','Stim 40'});

suptitle(sprintf('Results: %s', titleStr));
set(findall(gcf,'-property','FontSize'),'FontSize',15);
saveas(fig,[plot_path sprintf('%s_Results.png',titleStr)]);

end
