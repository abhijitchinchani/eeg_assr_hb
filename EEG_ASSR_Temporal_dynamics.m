clear; clc; close all hidden;

Toolbox_Path = pwd;
addpath(genpath([Toolbox_Path '/Code_library/Userdefined']));
InitToolboxPath(Toolbox_Path);

Subs = [1:42 44:46];
ASSR_freq = 40;
Noise_freq = 5;
freq_err = 0.1;

Fs = 1000;
SubType = readcell('Data_HB/Subject_type.csv');
SubType = [SubType{:,3}];
SubSham = find(SubType == 0);
SubStim40 = find(SubType == 40);
SubStim11 = find(SubType == 11);

analyse_flag = true;
save_path = sprintf('Data_HB/Temporal_dynamics/'); mkdir(save_path);
plot_path = sprintf('Plots/Temporal_dynamics/'); mkdir(plot_path);
k = 7; Tr_Avg = 10;

%% Load data and analyse

if analyse_flag
    
    for sub = 1:length(Subs)
        data_path = sprintf('Data_HB/Preprocess/HB_%d/',Subs(sub));
        
        SubData = load(fullfile(data_path,sprintf('HB_%d_Preproc.mat',Subs(sub))));
        SubCPCAData = load(fullfile(data_path,sprintf('HB_%d_CPCA_G_H.mat',Subs(sub))));

        Comps = nt_mmat(permute(SubData.TrialsT0, [2,1,3]), SubCPCAData.CPCA_GH_T0.VS(:,1:k));
        SNR = getSnrStruct(permute(Comps, [2,1,3]), ASSR_freq, Noise_freq, freq_err, Fs);
        n = SNR == max(SNR);
        TD_struct.GH_T0(sub) = getTemporalDynamics(Comps(n,:,:), Tr_Avg, ASSR_freq, Noise_freq, freq_err, Fs); 
        
        Comps = nt_mmat(permute(SubData.TrialsT1, [2,1,3]), SubCPCAData.CPCA_GH_T1.VS(:,1:k));
        SNR = getSnrStruct(permute(Comps, [2,1,3]), ASSR_freq, Noise_freq, freq_err, Fs);
        n = SNR == max(SNR);
        TD_struct.GH_T1(sub) = getTemporalDynamics(Comps(n,:,:), Tr_Avg, ASSR_freq, Noise_freq, freq_err, Fs); 
        
        Comps = nt_mmat(permute(cat(3, SubData.TrialsT0, SubData.TrialsT1), [2,1,3]), SubCPCAData.CPCA_GH_T0_T1.VS(:,1:k));
        SNR = getSnrStruct(permute(Comps, [2,1,3]), ASSR_freq, Noise_freq, freq_err, Fs);
        n = SNR == max(SNR);
        Comps = nt_mmat(permute(SubData.TrialsT0, [2,1,3]), SubCPCAData.CPCA_GH_T0_T1.VS(:,1:k));
        TD_struct.GH_comb_T0(sub) = getTemporalDynamics(Comps(n,:,:), Tr_Avg, ASSR_freq, Noise_freq, freq_err, Fs); 
        Comps = nt_mmat(permute(SubData.TrialsT1, [2,1,3]), SubCPCAData.CPCA_GH_T0_T1.VS(:,1:k));
        TD_struct.GH_comb_T1(sub) = getTemporalDynamics(Comps(n,:,:), Tr_Avg, ASSR_freq, Noise_freq, freq_err, Fs); 
        
        
        fprintf('CPCA GH Analysis: Sub %d Completed \n', Subs(sub));
    end
    
    save(fullfile(save_path, 'TemporalDynamics.mat'),'TD_struct');
    
end

%% Plots

load(fullfile(save_path, 'TemporalDynamics.mat'),'TD_struct');

data = {cat(1, SNR_struct.raw_T0), cat(1, SNR_struct.raw_T1)};
plotHeatMap(data, 'Ch', 'Sub', [0 0 1920 1080], {'T0', 'T1'}, 'Raw EEG', plot_path);

data = {cat(1, SNR_struct.GH_T0), cat(1, SNR_struct.GH_T1)};
plotHeatMap(data, 'Ch', 'Sub', [0 0 1920 1080], {'T0', 'T1'}, 'CPCA GH separate', plot_path);

data = {cat(1, SNR_struct.GH_comb_T0), cat(1, SNR_struct.GH_comb_T1)};
plotHeatMap(data, 'Ch', 'Sub', [0 0 1920 1080], {'T0', 'T1'}, 'CPCA GH combined', plot_path);

%% Functions

function TD = getTemporalDynamics(data, Tr_Avg, ASSR_freq, Noise_freq, freq_err, Fs)

data = squeeze(data)';
filtB = ones(1, Tr_Avg)./Tr_Avg;
data = filtfilt(filtB, 1, data);

params.Fs = Fs;
params.fpass = [2 55];
params.tapers = [1 1];
params.pad = -1;

for kk = 1:size(data,1)
    [S(:,kk), f(:,kk)] = mtspectrumc(data(kk,:), params);
end

f = f(:,1);
TD.SNR = S(abs(f-ASSR_freq) < freq_err,:)./nanmean(S(abs(f-ASSR_freq) > freq_err & abs(f-ASSR_freq) < Noise_freq,:),1);
TD.Pow = S(abs(f-ASSR_freq) < freq_err,:);

end

function SNR = getSnrStruct(data, ASSR_freq, Noise_freq, freq_err, Fs)

params.Fs = Fs;
params.fpass = [2 55];
params.tapers = [1 1];
params.pad = -1;

for kk = 1:size(data,1)
    [S(:,kk), f(:,kk)] = mtspectrumc(nanmean(data(kk,:,:), 3), params);
end

f = f(:,1);
SNR = S(abs(f-ASSR_freq) < freq_err,:)./nanmean(S(abs(f-ASSR_freq) > freq_err & abs(f-ASSR_freq) < Noise_freq,:),1);

end

function [] = plotHeatMap(data, XStr, YStr, Pos, titleStr, supStr, plot_path)

% data = Subjects x Channels/Components

fig = figure;
set(fig, 'Position', Pos);

for ii = 1:length(data)
    temp = log(data{ii});
%     temp = temp./nansum(temp,2);
    
    subplot(1, Col(1), Col(2));
    h = heatmap(temp); colormap hot;
    if ii == 1
        h.YData = sprintfc('%s {%d}', YStr, 1:size(data, 1));
    end
    h.XData = sprintfc('%s {%d}', XStr, 1:size(data, 1));
    h.ColorLimits = [0 1];
    title(titleStr{ii});
end

suptitle(sprintf('log SNR: %s', supStr));
set(findall(gcf,'-property','FontSize'),'FontSize',15);
saveas(fig,[plot_path sprintf('log_SNR_%s.png',strrep(supStr, ' ','_'))]);

end
