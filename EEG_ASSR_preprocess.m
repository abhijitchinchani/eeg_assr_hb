clear; clc; close all;

Toolbox_Path = pwd;
addpath(genpath([Toolbox_Path '/Code_library/Userdefined']));
InitToolboxPath(Toolbox_Path);

Subs = [1:42 44:46];

%% Load data and preprocess

Fs = 1000; 
TimeOfTrial = 2;
TimeBaseline = 1;

for sub = 1:length(Subs)
    data_path = sprintf('Data_HB/HB_%.2d/',Subs(sub));
    save_path = sprintf('Data_HB/Preprocess/HB_%d/',Subs(sub)); mkdir(save_path);
    plot_path = sprintf('Plots/Preprocess/HB_%d/',Subs(sub)); mkdir(plot_path);
    
    file_list = dir(fullfile(data_path, 'ASSR_HB_*pre*.raw'));
    if length(file_list) > 1
       error('Multiple files!!'); 
    end
    data = ft_read_data(fullfile(data_path, file_list(1).name));
    events = ft_read_event(fullfile(data_path, file_list(1).name));
    data = data(1:256,:);
    temp = Preprocess_Epoch(data, Fs, events, 'aeSt', [TimeBaseline,TimeOfTrial], ...
        'ch_interpolate_flag', true, 'ch_file', 'cog_GSN-HydroCel-256.sfp');
    
    TrialsT0 = temp(:,TimeBaseline*Fs+1:end,:);
    BaselineT0 = temp(:,1:TimeBaseline*Fs,:);
    
    file_list = dir(fullfile(data_path, 'ASSR_HB_*post*.raw'));
    if length(file_list) > 1
       error('Multiple files!!'); 
    end
    data = ft_read_data(fullfile(data_path, file_list(1).name));
    events = ft_read_event(fullfile(data_path, file_list(1).name));
    data = data(1:256,:);
    temp = Preprocess_Epoch(data, Fs, events, 'aeSt', [TimeBaseline,TimeOfTrial], ...
        'ch_interpolate_flag', true, 'ch_file', 'cog_GSN-HydroCel-256.sfp');
    
    TrialsT1 = temp(:,TimeBaseline*Fs+1:end,:);
    BaselineT1 = temp(:,1:TimeBaseline*Fs,:);
    
    if any(isnan(TrialsT0(:))) || any(isnan(TrialsT1(:)))
        error('NaN in data!!');
    end
    
    plotSpectrum(TrialsT0, Fs, plot_path, sprintf('HB_%d_ASSR_Spectrum_T0.png',Subs(sub)));
    plotSpectrum(TrialsT1, Fs, plot_path, sprintf('HB_%d_ASSR_Spectrum_T1.png',Subs(sub)));
    close all;
    
    fprintf('EEG Preprocessing: Sub %d Completed \n', Subs(sub));
    
    save([save_path sprintf('HB_%d_Preproc.mat',Subs(sub))],'Fs','TimeOfTrial','Trials*','Baseline*');
    clear Trials* Baseline* data events file_list temp; 
end


%% Functions

function [] = plotSpectrum(Trials, Fs, plot_path, filename)

fig = figure;
emo_plot_mt(nanmean(Trials(:,:,:),3),Fs,[2 50]);
xlabel('Frequency in Hz');
ylabel('Power');
title('Spectrum of ASSR at 40Hz');
set(findall(gcf,'-property','FontSize'),'FontSize',15)
saveas(fig,fullfile(plot_path, filename));

end
