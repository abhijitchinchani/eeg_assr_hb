clear; clc; close all;

Toolbox_Path = pwd;
addpath(genpath([Toolbox_Path '/Code_library/Userdefined']));
InitToolboxPath(Toolbox_Path);

Subs = [1:42 44:46];
fMax = 45; 
freq_arr = [2,4,8,14,25,39.9,40.1,45];
freq_labels = {'Delta (2-4Hz)', 'Theta (4-8Hz)', 'Alpha (8-14Hz)', 'Beta (14-25Hz)', 'Gamma (25-39Hz)', 'ASSR (40Hz)', 'High gamma (>40Hz)'};
k = length(freq_arr)-1;

%% Load data and preprocess

for sub = 1:length(Subs)
    data_path = sprintf('Data_HB/Preprocess/HB_%d/',Subs(sub)); 
    plot_path = sprintf('Plots/CPCA_G_H/HB_%d/',Subs(sub)); mkdir(plot_path);
    
    SubData = load(fullfile(data_path,sprintf('HB_%d_Preproc.mat',Subs(sub))));
    CPCA_G_T0 = CPCA_G_fft(SubData.TrialsT0, SubData.Fs, SubData.TimeOfTrial, fMax, freq_arr, k);
    plot_CPCA_params(CPCA_G_T0, plot_path, sprintf('Sub No %d t0 G',Subs(sub)), freq_labels);
    
    CPCA_G_T1 = CPCA_G_fft(SubData.TrialsT1, SubData.Fs, SubData.TimeOfTrial, fMax, freq_arr, k);
    plot_CPCA_params(CPCA_G_T1, plot_path, sprintf('Sub No %d t1 G',Subs(sub)), freq_labels);
    
    CPCA_G_T0_T1 = CPCA_G_fft(cat(3,SubData.TrialsT0,SubData.TrialsT1), SubData.Fs, SubData.TimeOfTrial, fMax, freq_arr, k);
    plot_CPCA_params(CPCA_G_T0_T1, plot_path, sprintf('Sub No %d t0+t1 G',Subs(sub)), freq_labels);
        
    CPCA_GH_T0 = CPCA_H(SubData.TrialsT0, CPCA_G_T0.VS(:,1:k), k);
    plot_CPCA_params(CPCA_GH_T0, plot_path, sprintf('Sub No %d t0 GH',Subs(sub)), freq_labels);
    
    CPCA_GH_T1 = CPCA_H(SubData.TrialsT1, CPCA_G_T1.VS(:,1:k), k);
    plot_CPCA_params(CPCA_GH_T1, plot_path, sprintf('Sub No %d t1 GH',Subs(sub)), freq_labels);
    
    CPCA_GH_T0_T1 = CPCA_H(cat(3,SubData.TrialsT0,SubData.TrialsT1), CPCA_G_T0_T1.VS(:,1:k), k);
    plot_CPCA_params(CPCA_GH_T0_T1, plot_path, sprintf('Sub No %d t0+t1 GH',Subs(sub)), freq_labels);
    
    save([data_path sprintf('HB_%d_CPCA_G_H.mat',Subs(sub))],'CPCA*');
    clear CPCA* SubData;
    
    fprintf('CPCA G H: Sub %d Completed \n', Subs(sub));
end

%% Functions

function CPCA_out = CPCA_G_fft(Trials, Fs, TimeOfTrial, fMax, freq_arr, k)

Z = permute(Trials(:,:,:), [2,1,3]); 
N = size(Z,1);
Sz = fft(Z.*hanning(N));
Sz = abs(Sz(1:round(N/2),:,:));
Sz = Sz - nanmean(Sz,1);

f = (0:1/TimeOfTrial:(Fs-1)/2)';
Sz = Sz(f<fMax,:,:); f = f(f<fMax);

Sz = permute(Sz, [1,3,2]);
Sz_big = reshape(Sz, [size(Sz,1)*size(Sz,2), size(Sz,3)]);
f_big = repmat(f,[size(Sz,2),1]);

G = zeros(length(f_big),length(freq_arr)-1);

for ii = 1:length(freq_arr)-1
    G(:,ii) = double(f_big>=freq_arr(ii) & f_big<freq_arr(ii+1));
end

[~,~,~, D] = runCPCA(Sz_big, 1, 1, k); 
[~,~,UG, DG, VG,~,~,UE, DE, VE] = runCPCA(Sz_big, G, 1, k); 

P_corr = corrcoef([UG(:,1:k), G]); 
P_corr_mat = P_corr(1:k,k+1:end); 

CPCA_out.US = UG;
CPCA_out.DS = DG;
CPCA_out.VS = VG;
CPCA_out.UE = UE;
CPCA_out.DE = DE;
CPCA_out.VE = VE;
CPCA_out.D = D;
CPCA_out.k = k;
CPCA_out.G = G;
CPCA_out.H = NaN;
CPCA_out.Pcorr = abs(P_corr_mat);

end

function CPCA_out = CPCA_H(Trials, H, k)

Z = permute(Trials(:,:,:), [2,3,1]); 
Z_time_big = reshape(Z,[size(Z,1)*size(Z,2),size(Z,3)]);

[~,~,~, D] = runCPCA(Z_time_big, 1, 1, k); 
[~,~,UH, DH, VH,~,~,UE, DE, VE] = runCPCA(Z_time_big, 1, H, k); 

P_corr = corrcoef([VH(:,1:k), H]); 
P_corr_mat = P_corr(1:k,k+1:end); 

CPCA_out.US = NaN; % Made NaN due to large size of US
CPCA_out.DS = DH;
CPCA_out.VS = VH;
CPCA_out.UE = NaN; % Made NaN due to large size of UE
CPCA_out.DE = DE;
CPCA_out.VE = VE;
CPCA_out.D = D;
CPCA_out.k = k;
CPCA_out.G = NaN;
CPCA_out.H = H;
CPCA_out.Pcorr = abs(P_corr_mat);

end

function [] = plot_CPCA_params(CPCA_struct, plot_path, fname, freq_labels)

k = CPCA_struct.k;

% Scree plot
fig = figure; 
plotScree(CPCA_struct.DS, 10);
set(findall(gcf,'-property','FontSize'),'FontSize',15);
saveas(fig,[plot_path sprintf('Scree_plot_%s.png',strrep(fname,' ','_'))]);

% Variance Table

[~, T] = buildVarianceTable(CPCA_struct.DS, CPCA_struct.DE, CPCA_struct.D, k);
fig = figure; 
set(fig, 'Position', [0 0 1920 1080]);
uitable(fig, 'Data',T{:,:},'ColumnName',T.Properties.VariableNames,...
    'RowName',T.Properties.RowNames,'Units', 'Normalized', 'Position',[0, 0, 1, 1]);
saveas(fig,[plot_path sprintf('Variance_table_%s.png',strrep(fname,' ','_'))]);

% Preditor correlation
fig = figure; 
set(fig, 'Position', [0 0 1920 1080]);
h = heatmap(abs(CPCA_struct.Pcorr));
Comp_labels = sprintfc('Comp %d', 1:k);
h.YData = Comp_labels;
h.XData = freq_labels;
colormap hot;
title('Prediction correlation');
set(findall(gcf,'-property','FontSize'),'FontSize',15)
saveas(fig,[plot_path sprintf('P_corr_%s.png',strrep(fname,' ','_'))]);

close all hidden;

end 