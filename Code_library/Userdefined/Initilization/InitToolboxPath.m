function [] = InitToolboxPath(Toolbox_Path)

restoredefaultpath;
addpath([Toolbox_Path '/Code_library/Toolbox/fieldtrip-20170817']);
addpath(genpath([Toolbox_Path '/Code_library/Toolbox/chronux_2_12']));
addpath(genpath([Toolbox_Path '/Code_library/Toolbox/NoiseTools']));
addpath(genpath([Toolbox_Path '/Code_library/Userdefined']));
addpath([Toolbox_Path '/Code_library/Toolbox/Misc']);
addpath(genpath([Toolbox_Path '/Code_library/Toolbox/kakearney-boundedline-pkg-38e849e']));
addpath([Toolbox_Path '/Code_library/Toolbox/SCADS']);
addpath([Toolbox_Path '/Code_library/Toolbox/eeglab14_0_0b']);
ft_defaults; 
eeglab; 
clear; close all hidden; clc;

end