function [S, f] = PlotCompH(data, W, Fs, k, Pcorr, freq_labels, varargin)

% data - Components x Time x Trials
% W - Weight matrix for components
% Fs - Sampling Freq
% k - No. of Components to plot

EEGLoc = 'cog_GSN-HydroCel-256.sfp';
plot_path = '';
titleStr = '';
fpass = [2 50];
assignopts(who,varargin{:});

for kk = 1:k
    fig = figure;
    set(fig, 'Position', [0 0 1980 1080]);
    
    s = subplot(3,2,1:2);
    h = heatmap(abs(Pcorr(kk,:))); colormap hot;
    h.YData = sprintf('Comp %d',kk);
    h.XData = freq_labels;
    h.ColorLimits = [0 1];
    
    subplot(3,2,3);
    [S(:,kk), f(:,kk)] = emo_plot_mt(nanmean(data(kk,:,:),3), Fs, fpass);
    xlabel('Freq in Hz'); ylabel('Power');
    title('Spectrum of average across trials');
    
    subplot(3,2,4);
    plot((1:size(data,2))/Fs, nanmean(data(kk,:,:),3));
    xlabel('Time in s'); ylabel('Amp'); 
    title('Time series of average across trials');
    
    subplot(3,2,5);
    emo_plot_mt_err(squeeze(data(kk,:,:))', Fs, fpass);
    xlabel('Freq in Hz'); ylabel('Power');
    title('Averaged spectrum across trials');
%     linkaxes(h);
    
    subplot(3,2,6);
    topoplot(W(:,kk),EEGLoc,'electrodes','on','colormap','jet');title('Electrode Weights');
    cbar;  % plot a colorbar
    
    suptitle(sprintf('%s Component %d', titleStr, kk));
    set(findall(gcf,'-property','FontSize'),'FontSize',12); 
    h.Colormap = hot;
    saveas(fig,[plot_path sprintf('%s_Comp_%d.png',titleStr, kk)]);
end

end