function s = PlotCompCol(data, W, Fs, col, varargin)

% data - Components x Time x Trials
% W - Weight matrix for components
% Fs - Sampling Freq
% k - No. of Components to plot

EEGLoc = 'cog_GSN-HydroCel-256.sfp';
titleStr = '';
fpass = [2 50];
s = gca;
assignopts(who,varargin{:});
row = 4; kk = 1;

s(1,col(2)) = subplot(row,col(1),col(2));
plot((1:size(data,2))/Fs, nanmean(data(kk,:,:),3));
xlabel('Time in s');
if col(2) == 1
    ylabel('Time Average');
end
title(titleStr);

s(2,col(2)) = subplot(row,col(1),col(1)+col(2));
[S(:,kk), f(:,kk)] = emo_plot_mt(nanmean(data(kk,:,:),3), Fs, fpass);
xlabel('Freq in Hz');
if col(2) == 1
    ylabel('Spectrum Average');
end

s(3,col(2)) = subplot(row,col(1),2*col(1)+col(2));
emo_plot_mt_err(squeeze(data(kk,:,:))', Fs, fpass);
xlabel('Freq in Hz'); 
if col(2) == 1
    ylabel('Average Spectrum');
end

s(4,col(2)) = subplot(row,col(1),3*col(1)+col(2));
topoplot(W(:,kk),EEGLoc,'electrodes','on','colormap','jet');title('Electrode Weights');
cbar;  % plot a colorbar


end