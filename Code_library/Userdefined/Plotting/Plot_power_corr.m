function [] = Plot_power_corr(P,SAVE)

LR = {'L','R'};

for lr = 1:length(LR)
    
    if lr == 1
       AT_idx = P(1).LeftFlick_T_idx; AA_idx = P(1).LeftFlick_D_idx; 
    else
       AT_idx = P(1).RightFlick_T_idx; AA_idx = P(1).RightFlick_D_idx; 
    end
    
    for i=1:length(P)
        temp = cat(1,P(i).power{AT_idx});
        dataAT{lr}(:,i) = temp(:,lr);
        temp = cat(1,P(i).power{AA_idx});
        dataAA{lr}(:,i) = temp(:,lr);
    end
    
    
    
end

%% Plot

f = figure;
set(f,'Position',[0 0 1080 1080]);

ATAA = {'AT','AA'};
LeftRight = {'Left','Right'};

N = length(P)-1;

for lr = 1:length(LR)
   for ataa = 1:2
       dataXY = eval(sprintf('data%s{lr}',ATAA{ataa}));
       for i = 1:N
          subplot(2,2*N,(ataa-1)*2*N + (lr-1)*N + i);
          plot_corr(log(dataXY(:,1)),log(dataXY(:,1+i)),true);
       end
       if ataa == 2
          xlabel(sprintf('DSS %s',LeftRight{lr})); 
       end
       if lr == 1
          ylabel(sprintf('DSS Att %s',ATAA{ataa})); 
       end
       
   end
end

set(findall(gcf,'-property','FontSize'),'FontSize',15)
saveas(f,[SAVE.savepath sprintf('Power_corr_%s.png',SAVE.fname)]);  
    

%% Functions

function [r,p] = plot_corr(data1,data2,str,plot_flag)

if ~exist('str','var')
    str = '';
end

if ~exist('plot_flag','var')
    plot_flag = true;
end

% for j=1:3
%     X = (data1 - median(data1))./std(data1); cut_off = 2;
%     data1(abs(X)>cut_off) = []; data2(abs(X)>cut_off) = [];
%     X = (data2 - median(data2))./std(data2);
%     data1(abs(X)>cut_off) = []; data2(abs(X)>cut_off) = [];
% end
[r,~,p] = bendcorr(data1,data2,0);

if plot_flag
    scatter(data1,data2,'Fill','FaceColor','r');
    axis square;
    title(sprintf('%s r = %.4f, p = %.4f',str,r,p));
end

end

end



