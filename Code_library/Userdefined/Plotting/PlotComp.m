function [S, f] = PlotComp(data, W, Fs, k, varargin)

% data - Components x Time x Trials
% W - Weight matrix for components
% Fs - Sampling Freq
% k - No. of Components to plot

EEGLoc = 'cog_GSN-HydroCel-256.sfp';
plot_path = '';
titleStr = '';
fpass = [2 50];
FigVisible = 'on';
assignopts(who,varargin{:});

for kk = 1:k
    fig = figure;
    set(fig, 'visible', FigVisible);
    set(fig, 'Position', [0 0 1280 1080]);
    
    subplot(2,2,1);
    [S(:,kk), f(:,kk)] = emo_plot_mt(nanmean(data(kk,:,:),3), Fs, fpass);
    xlabel('Freq in Hz'); ylabel('Power');
    title('Spectrum of average across trials');
    
    subplot(2,2,2);
    plot((1:size(data,2))/Fs, nanmean(data(kk,:,:),3));
    xlabel('Time in s'); ylabel('Amp'); 
    title('Time series of average across trials');
    
    subplot(2,2,3);
    emo_plot_mt_err(squeeze(data(kk,:,:))', Fs, fpass);
    xlabel('Freq in Hz'); ylabel('Power');
    title('Averaged spectrum across trials');
%     linkaxes(h);
    
    subplot(2,2,4);
    topoplot(W(:,kk),EEGLoc,'electrodes','on','colormap','jet');title('Electrode Weights');
    cbar;  % plot a colorbar
    
    suptitle(sprintf('%s Component %d', titleStr, kk));
    set(fig, 'visible', FigVisible);
    set(findall(gcf,'-property','FontSize'),'FontSize',12); 
    saveas(fig,[plot_path sprintf('%s_Comp_%d.png',titleStr, kk)]);
end

end