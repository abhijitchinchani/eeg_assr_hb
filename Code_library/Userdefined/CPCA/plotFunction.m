function [fig] = plotFunction(loadings, D, labelsStr, titleStr)

% This function plots the scree plot and the loading weights

fig = figure;
set(fig, 'Position', [0 0 1180 540]);

subplot(1,2,1);
plotScree(D);

subplot(1,2,2);
x = loadings(:,1); y = loadings(:,2);
plotScatter(x, y, labelsStr, titleStr);

end


