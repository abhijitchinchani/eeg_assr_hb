function [loadings_VD_sqrtN_GH, scores_NU_GH, UG, DGH, VGH, loadings_VD_sqrtN_E, scores_NU_E, UE, DE, VE] = runCPCA(X, G, H, n)

% X - rows are observations (Points), columns are features (variables)
% X should be zscored along the columns
% G - Predictor variable matrix along 1st dim -rows
% H - Predictor variable matrix along 2nd dim - cols
% n - Select top n components, if n is absent

N = size(X,1);
if ~all(size(G) == 1) && all(size(H) == 1)
    C = (G'*G)\G'*X; % Linear regression
    S = G*C; E = X - S;
elseif all(size(G) == 1) && ~all(size(H) == 1)
    B = X*H/(H'*H); % Linear regression
    S = B*H'; E = X - S;
elseif all(size(G) == 1) && all(size(H) == 1)
    S = X; E = X - S;
else
    return;
end

[UG, DGH, VGH] = svd(S,'econ'); % SVD for GC
[loadings_VD_sqrtN_GH, scores_NU_GH] = findLoadingsScores(UG, DGH, VGH, N, n);

[UE, DE, VE] = svd(E,'econ'); % SVD for E
[loadings_VD_sqrtN_E, scores_NU_E] = findLoadingsScores(UE, DE, VE, N, n);


end


function [loadings_VD_sqrtN, scores_NU] = findLoadingsScores(U, D, V, N, n)

loadings_VD_sqrtN = V*diag(diag(D))/sqrt(N-1);
scores_NU = N*U;

loadings_VD_sqrtN = loadings_VD_sqrtN(:,1:n);
scores_NU = scores_NU(:,1:n);

% Perform varimax rotation
% [loadings_VD_sqrtN, T]= varimkn_beh(loadings_VD_sqrtN);
% scores_NU = scores_NU*T;

end