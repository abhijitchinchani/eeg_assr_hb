function [] = plotScree(D, k)
var = diag(D).^2;
plot(1:k,var(1:k)./sum(var)*100,'-O');
xlabel('No. of Components');
ylabel('% variance explained by nth components');
title('Scree Plot');
end