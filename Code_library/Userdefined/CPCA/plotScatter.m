function [] = plotScatter(x, y, labelsStr, titleStr)

scatter(x,y,'*');
dx = 0.02; dy = 0.02;

if nargin > 3
    suptitle(titleStr);
    text(x+dx, y+dy, labelsStr);
elseif nargin > 2
    text(x+dx, y+dy, labelsStr);
end

xlabel('Component 1');
ylabel('Component 2');
axis equal; axis square;

end