function [Trials, goodSensors] = Preprocess_Epoch(data, Fs, events, eventStr, Time, varargin)

filt = designfilt('bandpassiir', 'StopbandFrequency1', 0.1, 'PassbandFrequency1', 2, 'PassbandFrequency2', 50, ...
    'StopbandFrequency2', 55, 'StopbandAttenuation1', 57, 'PassbandRipple', 1, 'StopbandAttenuation2', 60, 'SampleRate', Fs);

Rej_bad_ch_flag = true; 
Rej_bad_epoch_flag = false; 
demean_reref_flag = true;
z_score_flag = true;
ch_interpolate_flag = false;
ch_file = '';
goodSensors = 1:size(data,1);
rejectSensors = [];
n_interp_chs = 10;

assignopts(who, varargin);


dataFilt = filtfilt(filt, data')';
Trials = [];

% Reject Bad Channels
StartIdx = [events(strcmp({events.value},eventStr)).sample]; 
for tr = 1:length(StartIdx)
    Trials(:,:,tr) = dataFilt(:,StartIdx(tr)-Time(1)*Fs:StartIdx(tr)+Time(2)*Fs-1);
end

% Reject Bad Channels

if Rej_bad_ch_flag
    [pMaxAmp, pStdDev, pGradient] = cog_scads_1_2(permute(Trials,[2,1,3]));
    rejectSensorsMA = cog_scads_1_3(pMaxAmp, 3);
    rejectSensorsSD = cog_scads_1_3(pStdDev, 3);
    rejectSensorsGD = cog_scads_1_3(pGradient, 3);
    rejectSensors = unique([rejectSensorsMA' rejectSensorsSD' rejectSensorsGD']);
    goodSensors = setdiff(1:size(data,1),rejectSensors);
    Trials(rejectSensors,:,:) = NaN;
end

if Rej_bad_epoch_flag
    for ch = goodSensors
        rejectTrialsMA = cog_scads_1_3(pMaxAmp(ch,:)', 3);
        rejectTrialsSD = cog_scads_1_3(pStdDev(ch,:)', 3);
        rejectTrialsGD = cog_scads_1_3(pGradient(ch,:)', 3);
        
        rejectTrials = unique([rejectTrialsMA' rejectTrialsSD' rejectTrialsGD']);
        Trials(ch,:,rejectTrials) = NaN;
    end
end

if ch_interpolate_flag
    ch_loc = readlocs(ch_file);
    xyz_cood = [ch_loc.X; ch_loc.Y; ch_loc.Z];
    dist_mat = dist(xyz_cood);
    for ch = rejectSensors
       [~, neighbourSensors] = sort(dist_mat(ch,:),'ascend');
       neighbourSensors = neighbourSensors(2:n_interp_chs+1);
       Trials(ch,:,:) = nanmean(Trials(neighbourSensors,:,:),1); 
    end
end

if demean_reref_flag
    Trials = Trials - nanmean(Trials,2);  % Demean
    Trials = Trials - nanmean(Trials,1);  % Reref
end

if z_score_flag
    Trials = zscore(permute(Trials,[2,1,3]));
    Trials = permute(Trials, [2,1,3]);
end


end