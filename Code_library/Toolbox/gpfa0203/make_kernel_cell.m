function kernels = make_kernel_cell(Fs, SSVEP_freq, band_freq, uniform_freq, no_of_rbf)

cnt = 1;

for i=1:length(SSVEP_freq)
    kernels{cnt}.covType = 'cosine';
    kernels{cnt}.freq = SSVEP_freq(i).freq;
    kernels{cnt}.coeff = SSVEP_freq(i).coeff;
    kernels{cnt}.Fs = Fs;
    cnt = cnt +1;
end

for i=1:length(band_freq)
    kernels{cnt}.covType = 'expcos';
    kernels{cnt}.freq = band_freq(i).freq;
    kernels{cnt}.Fs = Fs;
    kernels{cnt}.sig = band_freq(i).sig;
    cnt = cnt +1;
end

for i=1:length(uniform_freq)
    kernels{cnt}.covType = 'uniform';
    kernels{cnt}.freq = uniform_freq(i).freq;
    kernels{cnt}.Fs = Fs;
    kernels{cnt}.sig = uniform_freq(i).sig;
    cnt = cnt +1;
end

for i=1:no_of_rbf
    kernels{cnt}.covType = 'rbf';
    cnt = cnt +1;
end


end