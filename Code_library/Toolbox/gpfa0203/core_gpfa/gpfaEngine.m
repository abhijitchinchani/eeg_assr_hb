function outstruct = gpfaEngine(seqTrain, seqTest, kernels, varargin)
%
% gpfaEngine(seqTrain, seqTest, fname, ...)
%
% Extract neural trajectories using GPFA.
%
% INPUTS:
%
% seqTrain      - training data structure, whose nth entry (corresponding to
%                 the nth experimental trial) has fields
%                   trialId (1 x 1)   -- unique trial identifier
%                   y (# neurons x T) -- neural data
%                   T (1 x 1)         -- number of timesteps
% seqTest       - test data structure (same format as seqTrain)
% fname         - filename of where results are saved
%
% OPTIONAL ARGUMENTS:
%
% xDim          - state dimensionality (default: 3)
% binWidth      - spike bin width in msec (default: 20)
% startTau      - GP timescale initialization in msec (default: 100)
% startEps      - GP noise variance initialization (default: 1e-3)
%
% @ 2009 Byron Yu         byronyu@stanford.edu
%        John Cunningham  jcunnin@stanford.edu

shufFlag     = false;
shuf          = 1:length(kernels);
assignopts(who, varargin);

xDim          = length(kernels);
binWidth      = 20; % in msec
startTau      = 100; % in msec
startEps      = 1e-3;
normalize_c   = false;
learnSigFlag  = false;
showFaFlag    = false;
saveFaDir     = [cd '/'];

extraOpts     = assignopts(who, varargin);

% For compute efficiency, train on equal-length segments of trials
seqTrainCut = cutTrials(seqTrain, extraOpts{:});
if isempty(seqTrainCut)
    fprintf('WARNING: no segments extracted for training.  Defaulting to segLength=Inf.\n');
    seqTrainCut = cutTrials(seqTrain, 'segLength', Inf);
end

% ==================================
% Initialize state model parameters
% ==================================

for i=1:xDim
    
    startParams.covType{i} = kernels(i).Type;
    
    switch startParams.covType{i}
        case 'rbf'
            % GP timescale
            % Assume binWidth is the time step size.
            startParams.gamma(i) = (binWidth / startTau)^2; %%REMOVE THIS RAND
            % GP noise variance
            startParams.eps(i)   = startEps;
            startParams.a(i) = startParams.eps(i);
            startParams.coeff{i} = NaN;
            startParams.freq{i} = [];
            startParams.Fs(i) = kernels(i).Fs;
            startParams.notes.learnKernelParams(i) = kernels(i).LearnParams;
        case 'expcos'
            % GP timescale
            % Assume binWidth is the time step size.
            startParams.gamma(i) = (1 * kernels(i).Params)^2;
            % GP noise variance
            startParams.eps(i)   = startEps;
            startParams.a(i) = startParams.eps(i);
            startParams.freq{i} = kernels(i).Freq;
            startParams.coeff{i} = NaN;
            startParams.Fs(i) = kernels(i).Fs;
            startParams.notes.learnKernelParams(i) = kernels(i).LearnParams;
        case 'uniform'
            % GP timescale
            % Assume binWidth is the time step size.
            startParams.gamma(i) = kernels(i).Params;
            % GP noise variance
            startParams.eps(i)   = startEps;
            startParams.a(i) = startParams.eps(i);
            startParams.freq{i} = kernels(i).Freq;
            startParams.coeff{i} = NaN;
            startParams.Fs(i) = kernels(i).Fs;
            startParams.notes.learnKernelParams(i) = kernels(i).LearnParams;
        case 'cosine'
            startParams.gamma(i) = NaN;
            % GP noise variance
            startParams.eps(i)   = startEps;
            startParams.a(i) = startParams.eps(i);
            startParams.freq{i} = kernels(i).Freq;
            startParams.coeff{i} = kernels(i).Params;
            startParams.Fs(i) = kernels(i).Fs;
            startParams.notes.learnKernelParams(i) = kernels(i).LearnParams;
        case 'expsin2'
            startParams.gamma(i) = 1/(kernels(i).Params)^2; %(kernels(i).sig)^2;  %% CHECK %%
            startParams.eps(i)   = startEps;
            startParams.a(i) = startParams.eps(i);
            startParams.freq{i} = kernels(i).Freq;
            startParams.coeff{i} = NaN;
            startParams.Fs(i) = kernels(i).Fs;
            startParams.notes.learnKernelParams(i) = kernels(i).LearnParams;
        case 'expsin2_Fr'
            startParams.gamma(i) = 1/(kernels(i).Params)^2; %(kernels(i).sig)^2;  %% CHECK %%
            startParams.eps(i)   = startEps;
            startParams.a(i) = startParams.eps(i);
            startParams.freq{i} = kernels(i).Freq;
            startParams.coeff{i} = NaN;
            startParams.Fs(i) = kernels(i).Fs;
            startParams.notes.learnKernelParams(i) = kernels(i).LearnParams;
        otherwise
            error('Wrong kernel!!');
            
    end
    
end
% ========================================
% Initialize observation model parameters
% ========================================
fprintf('Initializing parameters using factor analysis...\n');

yAll             = [seqTrainCut.y];
[faParams, faLL] = fastfa(yAll, xDim, extraOpts{:});
[fa_Z,~] = fastfa_estep(yAll, faParams);

if shufFlag == true
    faParams.L = faParams.L(:,shuf);
end

if showFaFlag == true
    noTrials = length(seqTrain); noSamples = length(seqTrainCut)*size(seqTrainCut(1).y,2)/noTrials;
    faComps = reshape(fa_Z.mean, [size(fa_Z.mean,1) noSamples noTrials]);
    
    for fr = 1:size(faComps,1)
        for tr = 1:size(faComps,3)
            faComps(fr,:,tr) = zscore(faComps(fr,:,tr));
        end
    end
    noLt = size(faComps,1);
    
    h1 = figure; h1.Color = [1 1 1]; h1.Position = [10 10 1300 820];
    for fr = 1:noLt
        disp(fr)
        ax1(fr) = subplot(noLt,4,(fr-1)*4+1); %subplot(2,4,(fr-1)*4+1);
        emo_plot_mt_err(squeeze(faComps(fr,:,:))', startParams.Fs(1));
        box off; %axis square; 
        xlabel('Frequency (Hz)');
        if fr == 1; title('Freq Spectrum avg'); end
        h1.CurrentAxes.FontSize = 12;
        h1.CurrentAxes.XLim = [0 45];
        h1.CurrentAxes.XTick = [0 startParams.freq{fr} 45];
        
        ax2(fr) = subplot(noLt,4,(fr-1)*4+2);
        emo_plot_mt_err(squeeze(nanmean(faComps(fr,:,:),3))', startParams.Fs(1));
        box off; %axis square;
        xlabel('Frequency (Hz)');
        if fr == 1; title('Freq Spectrum of Time avg'); end
        h1.CurrentAxes.FontSize = 12;
        h1.CurrentAxes.XLim = [0 45];
        h1.CurrentAxes.XTick = [0 startParams.freq{fr} 45];
        
        [Uw,Sw,Vw] = svd(faParams.L);
        faParams.U = Uw(:,1:size(faParams.L,2)); faParams.S = Sw(1:size(faParams.L,2),:); faParams.V = Vw;
        for tr = 1:size(faComps,3)
            faComps_orth(:,:,tr) = faParams.S*faParams.V'*faComps(:,:,tr);
        end
        
        ax3(fr) = subplot(noLt,4,(fr-1)*4+3);
        emo_plot_mt_err(squeeze(faComps_orth(fr,:,:))', startParams.Fs(1));
        box off; %axis square;
        xlabel('Frequency (Hz)');
        if fr == 1; title(sprintf('Orthonormalized \n Freq Spectrum avg')); end
        h1.CurrentAxes.FontSize = 12;
        h1.CurrentAxes.XLim = [0 45];
        h1.CurrentAxes.XTick = [0 startParams.freq{fr} 45];
        
        ax4(fr) = subplot(noLt,4,(fr-1)*4+4);
        emo_plot_mt_err(squeeze(nanmean(faComps_orth(fr,:,:),3))', startParams.Fs(1));
        box off; %axis square;
        xlabel('Frequency (Hz)');
        if fr == 1; title(sprintf('Orthonormalized \n Freq Spectrum of Time avg')); end
        h1.CurrentAxes.FontSize = 12;
        h1.CurrentAxes.XLim = [0 45];
        h1.CurrentAxes.XTick = [0 startParams.freq{fr} 45];
    end
    linkaxes(ax1,'xy'); linkaxes(ax2,'xy'); linkaxes(ax3,'xy'); linkaxes(ax4,'xy');
    
    saveas(h1,[saveFaDir 'Plot_FAlatents.png']);%,'-nocrop',h1);
    
    orthFlag = input('Enter 1:Orth, 0:Non-Orth ');
    if orthFlag; faParams.L = faParams.U; end
    
    shuf = input('Enter the order of the frequencies: ');
    faParams.L = faParams.L(:,shuf);
end

startParams.d = mean(yAll, 2);
startParams.C = faParams.L;
% startParams.C = rand(size(yAll,1),xDim);
startParams.R = diag(faParams.Ph);
% startParams.R = diag(rand(1,size(yAll,1)));

% Define parameter constraints

startParams.notes.learnGPNoise      = false;
startParams.notes.RforceDiagonal    = true;

currentParams = startParams;

% =====================
% Fit model parameters
% =====================
fprintf('\nFitting GPFA model...\n');

[estParams, seqTrainCut, LLcut, iterTime, estParams_1, LL_1, estParams_2, LL_2] =...
    em(currentParams, seqTrainCut, extraOpts{:});

% normalization of C
if normalize_c
    estParams.C = (estParams.C - repmat(min(estParams.C,[],1),[size(estParams.C,1) 1]))./...
        (repmat(max(estParams.C,[],1),[size(estParams.C,1) 1])-repmat(min(estParams.C,[],1),[size(estParams.C,1) 1]));
end
% Extract neural trajectories for original, unsegmented trials
% using learned parameters
[seq_out, LLtrain] = exactInferenceWithLL(seqTrain, estParams);

% ==================================
% Assess generalization performance
% ==================================
if ~isempty(seqTest) % check if there are any test trials
    % Leave-neuron-out prediction on test data
    if estParams.notes.RforceDiagonal
        seqTest = cosmoother_gpfa_viaOrth_fast(seqTest, estParams, 1:xDim);
    else
        seqTest = cosmoother_gpfa_viaOrth(seqTest, estParams, 1:xDim);
    end
    % Compute log-likelihood of test data
    [blah, LLtest] = exactInferenceWithLL(seqTest, estParams);
end

% =============
% Save results
% =============

clear yAll blah h1;
vars  = who;

for i = 1:length(vars)
    outstruct.(vars{i}) =  eval(vars{i});
end



% mt_params.Fs = startParams.Fs(1); mt_params.tapers = [1 1]; mt_params.fpass = [2 120];
% 
% freq_all = cell2mat(startParams.freq);
% freq_all = reshape(freq_all,[length(freq_all)/length(kernels), length(kernels)]);
% 
% for i = 1:xDim
%     [mt_S, mt_f] = mtspectrumc(fa_Z.mean(i,:), mt_params);
%     for j = 1:xDim
%         [~,tmp_indx] = min(abs(mt_f - freq_all(:,j)),[],2);
%         S_val(i,j) = sum(mt_S(unique(tmp_indx)));
%     end
% end