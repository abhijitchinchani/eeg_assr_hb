function res = learnGPparams(seq, params, varargin)
% Updates parameters of GP state model given neural trajectories.
%
% INPUTS:
%
% seq         - data structure containing neural trajectories
% params      - current GP state model parameters, which gives starting point
%               for gradient optimization
%
% OUTPUT:
%
% res         - updated GP state model parameters
%
% OPTIONAL ARGUMENTS:
%
% MAXITERS    - maximum number of line searches (if >0), maximum number
%               of function evaluations (if <0), for minimize.m (default:-8)
% verbose     - logical that specifies whether to display status messages
%               (default: false)
%
% @ 2009 Byron Yu         byronyu@stanford.edu
%        John Cunningham  jcunnin@stanford.edu

MAXITERS  = -8; % for minimize.m
verbose   = false;
assignopts(who, varargin);

xDim    = length(params.covType);

for i=1:xDim
    
    switch params.covType{i}
        case 'expsin2_Fr'
            oldParams(i) = params.gamma(i);
            fname{i} = 'grad_expsin2'; 
            fname_Fr{i} = 'grad_expsin2_Fr';
        case 'expsin2'
            oldParams(i) = params.gamma(i);
            fname{i} = 'grad_expsin2'; 
        case 'cosine'
            oldParams(i) = NaN;
            fname{i} = [];
        case 'expcos'
            oldParams(i) = params.gamma(i);
            fname{i}     = 'grad_expcos';
        case 'uniform'
            oldParams(i) = params.gamma(i);
            fname{i}     = 'grad_uniform';
        case 'rbf'
            % If there's more than one type of parameter, put them in the
            % second row of oldParams.
            oldParams(i) = params.gamma(i);
            fname{i}     = 'grad_betgam';
        case 'tri'
            oldParams(i) = params.a(i);
            fname{i}     = 'grad_trislope';
        case 'logexp'
            oldParams(i) = params.a(i);
            fname{i}     = 'grad_logexpslope';
    end
    
    if params.notes.learnGPNoise
        oldParams = [oldParams; params.eps];
        fname     = [fname{i} '_noise'];
    end
end


precomp = makePrecomp(seq, xDim);

% Loop once for each state dimension (each GP)
for i = 1:xDim
    const = [];
    
    if params.notes.learnKernelParams(i)
        
        switch params.covType{i}
            case 'expsin2_Fr'
                const.Fs = params.Fs(i);
                const.freq = params.freq{i};
                const.gamma = params.gamma(i);
            case 'expsin2'
                const.Fs = params.Fs(i);
                const.freq = params.freq{i};
            % No constants for 'rbf' or 'tri'
            case 'logexp'
                const.gamma = params.gamma(i);
            case 'expcos'
                const.mu = params.freq{i};
                const.Fs = params.Fs(i);
            case 'uniform'
                const.mu = params.freq{i};
                const.Fs = params.Fs(i);
        end
        if ~params.notes.learnGPNoise
            const.eps = params.eps(i);
        end
        
        initp = log(oldParams(:,i));

        % This does the heavy lifting
        [res_p, res_f, res_iters] =...
            minimize(initp, fname{i}, MAXITERS, precomp(i), const);
        
        if strcmp(params.covType{i},'expsin2_Fr')
            
            initp_Fr = log(params.freq{i});
            
            [res_p_Fr, res_f, res_iters] =...
                minimize(initp_Fr, fname_Fr{i}, MAXITERS, precomp(i), const);             
        end
        
        switch params.covType{i}
            case 'expsin2_Fr'
                res.gamma(i) = exp(res_p(1));
                res.freq{i} = exp(res_p_Fr(1));
            case 'expsin2'
                res.gamma(i) = exp(res_p(1));  
            case 'expcos'
                res.gamma(i) = exp(res_p(1));
            case 'uniform'
                res.gamma(i) = exp(res_p(1));
            case 'rbf'
                res.gamma(i) = exp(res_p(1));
            case 'tri'
                res.a(i)     = exp(res_p(1));
            case 'logexp'
                res.a(i)     = exp(res_p(1));
        end
        if params.notes.learnGPNoise
            res.eps(i) = exp(res_p(2));
        end
        
        if verbose
            fprintf('\nConverged p; xDim:%d, p:%s', i, mat2str(res_p, 3));
        end
    else
        res.a(i) = params.a(i);
        res.gamma(i)= params.gamma(i);
        res.freq{i} = params.freq{i};
    end
    
end
