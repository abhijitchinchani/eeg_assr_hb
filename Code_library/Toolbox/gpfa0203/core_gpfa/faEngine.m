function outstruct = faEngine(seqTrain, xDim, varargin)
%
% faEngine(seqTrain, xDim, varargin)
%
% Extract neural trajectories using GPFA.
%
% INPUTS:
%
% seqTrain      - training data structure, whose nth entry (corresponding to
%                 the nth experimental trial) has fields
%                   trialId (1 x 1)   -- unique trial identifier
%                   y (# neurons x T) -- neural data
%                   T (1 x 1)         -- number of timesteps
%
% @ 2009 Byron Yu         byronyu@stanford.edu
%        John Cunningham  jcunnin@stanford.edu

shufFlag     = false;
shuf          = 1:xDim;
assignopts(who, varargin);
showFaFlag    = false;
saveFaDir     = [cd '\'];
Fs            = 128;
freq          = 10;
extraOpts     = assignopts(who, varargin);

% For compute efficiency, train on equal-length segments of trials
seqTrainCut = cutTrials(seqTrain, extraOpts{:});
if isempty(seqTrainCut)
    fprintf('WARNING: no segments extracted for training.  Defaulting to segLength=Inf.\n');
    seqTrainCut = cutTrials(seqTrain, 'segLength', Inf);
end

% ========================================
% Initialize observation model parameters
% ========================================
fprintf('Initializing parameters using factor analysis...\n');

yAll             = [seqTrainCut.y];
[faParams, faLL] = fastfa(yAll, xDim, extraOpts{:});
[fa_Z,~] = fastfa_estep(yAll, faParams);

if shufFlag == true
    faParams.L = faParams.L(:,shuf);
end

if showFaFlag == true
    noTrials = length(seqTrain); noSamples = length(seqTrainCut)*size(seqTrainCut(1).y,2)/noTrials;
    faComps = reshape(fa_Z.mean, [size(fa_Z.mean,1) noSamples noTrials]);
    
    for fr = 1:size(faComps,1)
        for tr = 1:size(faComps,3)
            faComps(fr,:,tr) = zscore(faComps(fr,:,tr));
        end
    end
    noLt = size(faComps,1);
    
    h1 = figure; h1.Color = [1 1 1]; h1.Position = [10 10 1300 820];
    for fr = 1:noLt
        ax1(fr) = subplot(noLt,4,(fr-1)*4+1); %subplot(2,4,(fr-1)*4+1);
        emo_plot_mt_err(squeeze(faComps(fr,:,:))', Fs);
        box off; %axis square; 
        xlabel('Frequency (Hz)');
        if fr == 1; title('Freq Spectrum avg'); end
        h1.CurrentAxes.FontSize = 12;
        h1.CurrentAxes.XLim = [0 45];
        h1.CurrentAxes.XTick = [0 unique(freq) 45];
        
        ax2(fr) = subplot(noLt,4,(fr-1)*4+2);
        emo_plot_mt_err(squeeze(nanmean(faComps(fr,:,:),3))', Fs);
        box off; %axis square;
        xlabel('Frequency (Hz)');
        if fr == 1; title('Freq Spectrum of Time avg'); end
        h1.CurrentAxes.FontSize = 12;
        h1.CurrentAxes.XLim = [0 45];
        h1.CurrentAxes.XTick = [0 unique(freq) 45];
        
        [Uw,Sw,Vw] = svd(faParams.L);
        faParams.U = Uw(:,1:size(faParams.L,2)); faParams.S = Sw(1:size(faParams.L,2),:); faParams.V = Vw;
        for tr = 1:size(faComps,3)
            faComps_orth(:,:,tr) = faParams.S*faParams.V'*faComps(:,:,tr);
        end
        
        ax3(fr) = subplot(noLt,4,(fr-1)*4+3);
        emo_plot_mt_err(squeeze(faComps_orth(fr,:,:))', Fs);
        box off; %axis square;
        xlabel('Frequency (Hz)');
        if fr == 1; title(sprintf('Orthonormalized \n Freq Spectrum avg')); end
        h1.CurrentAxes.FontSize = 12;
        h1.CurrentAxes.XLim = [0 45];
        h1.CurrentAxes.XTick = [0 unique(freq) 45];
        
        ax4(fr) = subplot(noLt,4,(fr-1)*4+4);
        emo_plot_mt_err(squeeze(nanmean(faComps_orth(fr,:,:),3))', Fs);
        box off; %axis square;
        xlabel('Frequency (Hz)');
        if fr == 1; title(sprintf('Orthonormalized \n Freq Spectrum of Time avg')); end
        h1.CurrentAxes.FontSize = 12;
        h1.CurrentAxes.XLim = [0 45];
        h1.CurrentAxes.XTick = [0 unique(freq) 45];
    end
    linkaxes(ax1,'xy'); linkaxes(ax2,'xy'); linkaxes(ax3,'xy'); linkaxes(ax4,'xy');
    
    export_fig([saveFaDir 'Plot_FAlatents.png'],'-nocrop',h1);
    
    orthFlag = input('Enter 1:Orth, 0:Non-Orth ');
    if orthFlag 
        faParams.L = faParams.U;
        faComps = faComps_orth;
    end
    
    shuf = input('Enter the order of the frequencies: ');
    faParams.L = faParams.L(:,shuf);
    faComps = faComps(shuf,:,:);
end


clear yAll blah;
vars  = who;

for i = 1:length(vars)
    outstruct.(vars{i}) =  eval(vars{i});
end
