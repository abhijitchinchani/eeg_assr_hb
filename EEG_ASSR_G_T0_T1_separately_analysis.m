clear; clc; close all;

Toolbox_Path = pwd;
addpath(genpath([Toolbox_Path '/Code_library/Userdefined']));
InitToolboxPath(Toolbox_Path);

Subs = 1:16;
freq_labels = {'Delta (2-4Hz)', 'Theta (4-8Hz)', 'Alpha (8-14Hz)', 'Beta (14-25Hz)', 'Gamma (25-39Hz)', 'ASSR (40Hz)', 'High gamma (>40Hz)'};
ASSR_freq = 40;
Noise_freq = 5;
freq_err = 0.1;

%% Load data and preprocess

for sub = 1:length(Subs)
    data_path = sprintf('Data/Preprocess/Pilot_%d/',Subs(sub)); 
    plot_path = sprintf('Plots/CPCA_G_T0_T1_separately/Pilot_%d/',Subs(sub)); mkdir(plot_path);
    
    SubData = load(fullfile(data_path,sprintf('Pilot_%d_Preproc.mat',Subs(sub))));
    SubCPCAData = load(fullfile(data_path,sprintf('Pilot_%d_CPCA_G_H.mat',Subs(sub))));
    
    k = SubCPCAData.CPCA_G_T0.k;
    W = SubCPCAData.CPCA_G_T0.VS(:,1:k);
    Pcorr = SubCPCAData.CPCA_G_T0.Pcorr;
    Comps = nt_mmat(permute(SubData.TrialsT0, [2,1,3]), SubCPCAData.CPCA_G_T0.VS(:,1:k));
    [S, f] = PlotCompH(permute(Comps,[2,1,3]), W, SubData.Fs, k, Pcorr, freq_labels,...
        'titleStr',sprintf('Pilot %d T0 G',Subs(sub)),'plot_path', plot_path);
    f = f(:,1); 
    SNR = S(abs(f-ASSR_freq) < freq_err,:)./nanmean(S(abs(f-ASSR_freq) > freq_err & abs(f-ASSR_freq) < Noise_freq,:),1);
    n = find(SNR == max(SNR)); close all;
    AvgComp(:,sub,1) = zscore(nanmean(Comps(:,n,:),3));
    AvgW(:,sub,1) = W(:,n);
    SelComp(sub,1) = n;
    
    k = SubCPCAData.CPCA_G_T1.k;
    W = SubCPCAData.CPCA_G_T1.VS(:,1:k);
    Pcorr = SubCPCAData.CPCA_G_T1.Pcorr;
    Comps = nt_mmat(permute(SubData.TrialsT1, [2,1,3]), SubCPCAData.CPCA_G_T1.VS(:,1:k));
    [S, f] = PlotCompH(permute(Comps,[2,1,3]), W, SubData.Fs, k, Pcorr, freq_labels,...
        'titleStr',sprintf('Pilot %d T1 G',Subs(sub)),'plot_path', plot_path);
    f = f(:,1); 
    SNR = S(abs(f-ASSR_freq) < freq_err,:)./nanmean(S(abs(f-ASSR_freq) > freq_err & abs(f-ASSR_freq) < Noise_freq,:),1);
    n = find(SNR == max(SNR)); close all;
    AvgComp(:,sub,2) = zscore(nanmean(Comps(:,n,:),3));
    AvgW(:,sub,2) = W(:,n);
    SelComp(sub,2) = n;
    
    clear SubData SubCPCAData;
    fprintf('CPCA G Analysis: Sub %d Completed \n', Subs(sub));
end

save_path = sprintf('Data/CPCA_G_T0_T1_separately/Average_Sub/'); mkdir(save_path);
save(fullfile(save_path, 'SelectedComponents.mat'),'AvgComp','AvgW','SelComp');

%% Subject average

for sub = 1:length(Subs)
    for ii = 1:2
        w = AvgW(:,sub,ii);
        [~, idx] = max(abs(w));
        AvgComp(:,sub,ii) = AvgComp(:,sub,ii)*sign(w(idx));
        AvgW(:,sub,ii) = AvgW(:,sub,ii)*sign(w(idx));
    end
end

Fs = 1000;
plot_path = sprintf('Plots/CPCA_G_T0_T1_separately/Average_Sub/'); mkdir(plot_path);
PlotComp(permute(AvgComp(:,:,1),[3,1,2]), nanmean(AvgW(:,:,1),2), Fs, 1, 'titleStr',sprintf('Average across subjects T0'),'plot_path', plot_path);
PlotComp(permute(AvgComp(:,:,2),[3,1,2]), nanmean(AvgW(:,:,2),2), Fs, 1, 'titleStr',sprintf('Average across subjects T1'),'plot_path', plot_path);

